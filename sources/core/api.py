from django.conf.urls import patterns, url
from flavors import views

urlpatterns = patterns("",
                       # {% url "api:flavors" %}
                       url(
                           regex=r"flavors/$",
                           view=views.FlavorCreateReadView.as_view(),
                           name="flavors"
                       ),
                       # {% url "api:flavors" flavor.slug %}
                       url(
                           regex=r"flavors/(?P<slug>[-\w]+)/$",
                           view=views.FlavorReadupdateDeleteView.as_view(),
                           name="flavors"
                       )
)