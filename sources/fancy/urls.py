from django.conf.urls import patterns, include, url

from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns('',
                       # Examples:
                       # url(r'^$', 'fancy.views.home', name='home'),
                       # url(r'^blog/', include('blog.urls')),

                       url(r'^admin/', include(admin.site.urls)),
                       url(r'^accounts/', include('allauth.urls')),
                       #url(r'^api/', include('flavors.urls')),
                       url(r'^api/', include('core.api'), name="api"),
                       url(r'^todo/', include('todo.urls'), name='todo')
)
