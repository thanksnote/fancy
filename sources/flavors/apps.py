__author__ = 'Justin'

from django.apps import AppConfig

class FlavorsConfig(AppConfig):
    name = 'flavors'
    verbose_name = "flavors"