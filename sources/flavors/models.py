from django.db import models
from django.core.urlresolvers import reverse

class Flavor(models.Model):
    title = models.CharField(max_length=200)
    slug = models.SlugField()
    scoop_remaining = models.IntegerField(default=0)

    def get_absolute_url(self):
        return reverse("api:flavors", kwargs={"slug": self.slug})
