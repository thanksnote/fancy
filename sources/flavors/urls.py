from django.conf.urls import patterns, url
from views import *

urlpatterns = patterns("",
                       url(
                           regex=r"api/$",
                           view=FlavorCreateReadView.as_view(),
                           name="flavor_rest_api"
                       ),
                       url(
                           regex=r"api/(?P<slug>[-\w]+)/$",
                           view=FlavorReadupdateDeleteView.as_view(),
                           name="flavor_rest_api"
                       )
)