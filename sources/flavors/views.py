from django.shortcuts import render

from rest_framework.generics import (
    ListCreateAPIView,
    RetrieveUpdateDestroyAPIView
)

from flavors.models import *

class FlavorCreateReadView(ListCreateAPIView):
    model = Flavor

class FlavorReadupdateDeleteView(RetrieveUpdateDestroyAPIView):
    model = Flavor