from django.contrib import admin

from todo.models import *

admin.site.register([Project, Repeat, Item, ItemTag, ProjectTag])
