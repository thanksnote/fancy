__author__ = 'Justin'

from django.apps import AppConfig

class TodoConfig(AppConfig):
    name = 'todo'
    verbose_name = "todo"