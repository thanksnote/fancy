from django.db import models


class Project(models.Model):
    id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=150)
    note = models.TextField(null=True)
    due_date = models.DateTimeField(null=True)
    user_id = models.PositiveIntegerField(null=False)
    done = models.BooleanField(default=False)
    order = models.PositiveIntegerField(null=False)


class Repeat(models.Model):
    DAILY = "DA"
    WEEKLY = "WE"
    MONTHLY = "MO"
    YEARLY = "YE"
    TERMS = (
        (DAILY, 'Daily'),
        (WEEKLY, 'Weekly'),
        (MONTHLY, 'Monthly'),
        (YEARLY, 'Yearly')
    )

    MONDAY = 0
    TUESDAY = 1
    WEDNESDAY = 2
    THURSDAY = 3
    FRIDAY = 4
    SATURDAY = 5
    SUNDAY = 6
    WEEKDAYS = (
        (MONDAY, "Monday"),
        (TUESDAY, "Tuesday"),
        (WEDNESDAY, "Wednesday"),
        (THURSDAY, "Thursday"),
        (FRIDAY, "Friday"),
        (SATURDAY, "Saturday"),
        (SUNDAY, "Sunday")
    )

    JANUARY = 1
    FEBRUARY = 2
    MARCH = 3
    APRIL = 4
    MAY = 5
    JUNE = 6
    JULY = 7
    AUGUST = 8
    SEPTEMBER = 9
    OCTOBER = 10
    NOVEMBER = 11
    DECEMBER = 12
    MONTHS = (
        (JANUARY, "January"),
        (FEBRUARY, "February"),
        (MARCH, "March"),
        (APRIL, "April"),
        (MAY, "May"),
        (JUNE, "June"),
        (JULY, "July"),
        (AUGUST, "August"),
        (SEPTEMBER, "September"),
        (OCTOBER, "October"),
        (NOVEMBER, "November"),
        (DECEMBER, "December"),
    )

    id = models.AutoField(primary_key=True)
    user_id = models.PositiveIntegerField(null=False)
    type = models.CharField(max_length=1, choices=TERMS, default=None)
    every = models.PositiveIntegerField(max_length=4)
    dayOfWeek = models.PositiveSmallIntegerField(choices=WEEKDAYS)
    day = models.PositiveIntegerField(null=None)
    month = models.PositiveSmallIntegerField(choices=MONTHS)
    year = models.PositiveSmallIntegerField(null=None)
    lastExcuted = models.DateField(blank=True, null=True)
    ends_on_date = models.DateTimeField(blank=True, null=True)
    ends_after_times = models.DateField(null=None)


class Item(models.Model):
    INBOX = "IN"
    REVIEW = "RE"
    TODAY = "TO"
    LABELS = (
        (INBOX, 'Inbox'),
        (REVIEW, 'Review'),
        (TODAY, 'Today')
    )

    id = models.AutoField(primary_key=True)
    project = models.ForeignKey(Project)
    title = models.CharField(max_length=150)
    note = models.TextField()
    repeat = models.ForeignKey(Repeat)
    user_id = models.PositiveIntegerField(null=False)
    due_date = models.DateTimeField()
    done = models.BooleanField(default=False)
    label = models.CharField(max_length=2,
                             choices=LABELS,
                             default=INBOX)
    order = models.PositiveIntegerField(null=False)


class ItemTag(models.Model):
    id = models.AutoField(primary_key=True)
    item = models.ForeignKey(Item)
    tag = models.CharField(max_length=50)


class ProjectTag(models.Model):
    id = models.AutoField(primary_key=True)
    project = models.ForeignKey(Project)
    tag = models.CharField(max_length=50)