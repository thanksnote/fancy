var smartApp = angular.module('smartApp', [
  	'ngRoute',
    'ngCookies',
    'ngResource',
  	//'ngAnimate', // this is buggy, jarviswidget will not work with ngAnimate :(
  	'ui.bootstrap',
  	'plunker',
  	'app.controllers',
    'app.services',
  	'app.demoControllers',
  	'app.main',
  	'app.navigation',
  	'app.localize',
  	'app.activity',
  	'app.smartui',
    'fancytodo.ui'
]);

smartApp.config(['$routeProvider', '$provide', '$httpProvider',  function($routeProvider, $provide, $httpProvider) {
	$routeProvider
		.when('/', {
			redirectTo: '/dashboard'
		})
		.when('/inbox', {
			templateUrl: function($routeParams) {
				return '/static/includes/inbox.html';
			},
			controller: 'FancyTodoController'
		})
        .when('/today', {
			templateUrl: function($routeParams) {
				return '/static/includes/today.html';
			},
			controller: 'FancyTodoController'
		})
		.otherwise({
			redirectTo: '/today'
		})
	;

	// with this, you can use $log('Message') same as $log.info('Message');
	$provide.decorator('$log', function($delegate) {
        // create a new function to be returned below as the $log service (instead of the $delegate)
        function logger() {
            // if $log fn is called directly, default to "info" message
            logger.info.apply(logger, arguments);
        }
        // add all the $log props into our new logger fn
        angular.extend(logger, $delegate);
        return logger;
    });

}]);



smartApp.run(['$rootScope', '$cookies', '$http', 'settings', function($rootScope, $cookies, $http, settings) {
	settings.currentLang = settings.languages[0]; // en
    $http.defaults.headers.common['X-CSRFToken'] = $cookies.csrftoken;
    $http.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
}])