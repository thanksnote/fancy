angular.module('app.services', [])
    .factory("FancyTodo", ['$resource', function ($resource) {
        return $resource("/todo/", {}, {
            'save': {
                url: '/todo/save',
                method: 'POST'
            }
        });
    }
    ]);