from django.conf.urls import patterns, url
from todo.views import *



urlpatterns = patterns('todo.views',
                       url(regex=r'^$', view='today'),
                       url(regex=r'^save', view='save')
)